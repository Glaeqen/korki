#include "oop.h"
int main() {
  typedef std::vector<Ct_double> myVec;
  myVec v;
  oop::fill(v, myVec::__TYPE__(1.14), myVec::__TYPE__(10)); // 1
  oop::stl_print_el(v, "v = ", ", "); // 2
  oop::for_each(v.begin(), v.end(), oop::op<myVec::__TYPE__>(10)); // 3
  oop::stl_print_el(v, "v = ", ", "); // 4
}
/* wynik:
   v = 1, 2, ..., 9
   v = 11, 12, ... 19
*/
