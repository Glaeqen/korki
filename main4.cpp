#include "final.h"
#include "non_copyable.h"
struct A : public final { };
struct B : public non_copyable { };
int main() {
  final _final; // ok
  A _a; // blad
  B _b; // ok
  B __b = _b; // blad
}
