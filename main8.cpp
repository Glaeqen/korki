#include "weird_templates.h"
struct A {};
int main()
{
  ASSERT(is_ptr<A*>::value); //ok bo w () jest A*
  ASSERT(is_const<const A*>::value); // ok bo w () jest const
  ASSERT(is_ptr<A>::value); //źle bo w () jest A
  ASSERT(is_const<A*>::value); //źle bo w () jest A*
  ASSERT(is_const<A>::value); //źle bo w () jest A
  ASSERT(is_ptr<remove_ptr<A*>::value_type*>::value); // dobrze bo w () jest A*
  ASSERT(is_ptr<remove_ptr<A*>::value_type>::value); // źle bo w () jest A
  ASSERT(is_const<const remove_const<const A>::value_type>::value); // tu chyba było jakoś inaczej, w każdym
  // razie miało działać :D
  ASSERT(is_const< remove_const<A>::value_type>::value); // miało nie działać
}
