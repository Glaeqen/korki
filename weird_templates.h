#pragma once
#include <iostream>

#define ASSERT(isOk)                            \
  {                                             \
    if(isOk){                                   \
      std::cout << "True  : " << std::flush;    \
    }                                           \
    else{                                       \
      std::cout << "False : " << std::flush;    \
    }                                           \
    std::cout << #isOk << std::endl;            \
  }                                             \

template
<typename T>
struct is_ptr{
  static const bool value = false;
};

template
<typename T>
struct is_ptr<T*>{
  static const bool value = true;
};

template
<typename T>
struct remove_ptr{
  typedef T value_type;
};

template
<typename T>
struct remove_ptr<T*>{
  typedef T value_type;
};

template
<typename T>
struct remove_const{
  typedef T value_type;
};

template
<typename T>
struct remove_const<const T>{
  typedef T value_type;
};

template
<typename T>
struct is_const{
  static const bool value = false;
};

template
<typename T>
struct is_const<const T>{
  static const bool value = true;
};

template
<typename T>
struct is_const<T*>{
  // Ta dodatkowa częściowa specjalizacja powinna rekurencyjnie
  // usuwać wskaźnikowość
  static const bool value = is_const<T>::value;
};
