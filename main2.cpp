#include <iostream>
namespace oop{
  template
  <typename T>
  struct Array{
    T *array = nullptr;
    int size = 0;
    int capacity = 0;
    Array(int capacity):array(new T[capacity]),size(0),capacity(capacity){}
    Array<T>& insert(const T& t){
      array[size] = t;
      size++;
      return *this;
    }
    T& operator[](int index){
      return array[index];
    }
    static T value_type(const T& t){
      return t;
    }
    Array<T>& operator%(const T& t){
      return insert(t);
    }
    int operator~(){
      return size;
    }
    ~Array(){
      if(array){
        delete[] array;
        array = nullptr;
        size = 0;
      }
    }

  };
}
/* uzupełnij */
namespace std{ class vector{}; class deque{}; class list{}; class multiset{}; class set{}; class map{}; class
multimap{}; }

#include <cstdlib> //do rand

int main()
{
  typedef oop::Array<char> type;
  type a(rand() %10 +5); //max rozmiar tablicy
  a.insert('#').insert('C') % type::value_type('P') % type::value_type('P') % '&';
  for(unsigned int i=0; i != ~a; ++i)
    std::cout << i << ":" << a[i] << ( (i +1) != ~a ? " " : "\n");
  return 0;
}
//output: 0:# 1:C 2:P 3:P 4:& 
