#include <iostream>
#include <utility>
#include "smart_ptr.h"
#include "dynamic_cast.h"

int main(){
  //
  smart_ptr<Test_Struct> ptr = new Test_Struct;
  ptr->variable = 55;
  std::cout << ptr->variable << std::endl;
  //
  A *a = new B; // No problem
  // Zawsze możesz castować klasę pochodną na bazową - podstawa polimorfizmu
  // B *b = a; // Problem;
  B *b = dynamic_cast<B*>(a);
  // Dynamic cast dokonuje sprawdzenia w runtime czy typ rzutowany jest faktycznie
  // typem pochodnym tj. czy 'a' (typ: A*) czy jest faktycznie B*
  // Dynamic cast - w przeciwieństwie do static casta - zwróci 0 jeśli rzutowanie się
  // nie powiedzie:
  B *nonB = dynamic_cast<B*>(new A); // A jest ogólniejsze niż B i nie ma z nim nic
  // wspólnego
  std::cout << a << std::endl;
  std::cout << b << std::endl;
  std::cout << nonB << std::endl;
}
