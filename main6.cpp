class A {
public:
  virtual void a() {}
  static void s() {}
};
class B: public A {
public:
  virtual void b() {}
};

int main(){
  const A a;
  // A& b = B(); // Źle, rvalue przypisane do l-value reference
  // const A& b = B(); // Ok, o ile wołasz metody const na 'b'
  // lub
  B some_b;
  A& b = some_b; // Ok, lvalue przypisane do l-value reference
  // a.a(); // Źle, obiekt typu const A, zawołanie metody A::a() (non-const)
  // a.b(); // Źle, obiekt typu A nie posiada metody A::b()
  a.s(); // Ok, zawołanie metody statycznej z instancji klasy, nie szkodzi
  b.a(); // Ok, obiekt typu A, zawołanie metody A::a()
  // b.b(); // Źle, obiekt typu A nie posiada metody A::b()
  b.s(); // Ok, zawołanie metody statycznej z instancji klasy, nie szkodzi
}
