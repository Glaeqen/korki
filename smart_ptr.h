#pragma once

struct Test_Struct {
  int variable;
};

template
<typename T>
class smart_ptr{
  T *ptr = nullptr;
public:
  smart_ptr() = default;
  smart_ptr(T*&& ptr):ptr(ptr){}
  smart_ptr(const smart_ptr<T>& ptr):ptr(ptr.ptr){}
  T& operator*(){
    return *ptr;
  }
  T* operator->(){
    return ptr;
  }
  ~smart_ptr(){
    if(ptr){
      delete ptr;
      ptr = nullptr;
    }
  }
};
