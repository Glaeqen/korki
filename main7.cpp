#include <iostream>
#include <vector>
struct Base{
  virtual ~Base(){}
};

struct Derived1 : Base{
  void derived1(){
    std::cout << "Derived1" << std::endl;
  }
};
struct Derived2 : Base{
  void derived2(){
    std::cout << "Derived2" << std::endl;
  }
};
struct Derived3 : Base{
  void derived3(){
    std::cout << "Derived3" << std::endl;
  }
};

int main(){
  std::vector<Base*> vec;
  vec.push_back(new Derived1);
  vec.push_back(new Base);
  vec.push_back(new Derived3);
  vec.push_back(new Derived2);
  vec.push_back(new Derived1);
  vec.push_back(new Base);

  // Oops, straciłem informację o tym jakiego typu są te pola.
  for(int i=0; i<vec.size(); i++){
    std::cout << "Index: " << i << std::endl;
    if(Derived1* f = dynamic_cast<Derived1*>(vec[i])){
      f->derived1();
      continue;
    }
    if(Derived2* f = dynamic_cast<Derived2*>(vec[i])){
      f->derived2();
      continue;
    }
    if(Derived3* f = dynamic_cast<Derived3*>(vec[i])){
      f->derived3();
      continue;
    }
    std::cout << "Base" << std::endl;
  }
}
