#pragma once

struct non_copyable {
  non_copyable(){}
  // C++11+
  /* non_copyable(const non_copyable&) = delete; */
  /* non_copyable& operator=(const non_copyable&) = delete; */
  // C++03:
private:
  non_copyable(const non_copyable&);
  non_copyable& operator=(const non_copyable&);
};
